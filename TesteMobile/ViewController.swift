//
//  ViewController.swift
//  TesteMobile
//
//  Created by Vitor Zitti on 7/12/18.
//  Copyright © 2018 Zitti. All rights reserved.
//

import UIKit

//Global Variables
var savedNumbers : [Int] = []
var savedResults : [Bool] = []
var savedArrays : [[Int]] = []

class ViewController: UIViewController, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {

    let jsonUrl : String = "https://k9h52hczu2.execute-api.us-east-2.amazonaws.com/prod/random_array_generator"
    
    var arrayOfNumbers : [Int] = []
    
    @IBOutlet weak var arrayCollectionView: UICollectionView!
   
    @IBOutlet weak var textField: UITextField!
    @IBOutlet weak var buttonsView: UIView!
    
    var keyBoardShown : Bool = false
    var viewOrigin : CGFloat = 0
    
    let minValue = -99
    let maxValue = 99
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.arrayCollectionView.delegate = self
        self.arrayCollectionView.dataSource = self
        
        NotificationCenter.default.addObserver(self, selector: #selector(ViewController.keyboardWillShow), name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(ViewController.keyboardWillHide), name: NSNotification.Name.UIKeyboardWillHide, object: nil)
        
        let tapToDismiss: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(ViewController.dismissKeyboard))
        view.addGestureRecognizer(tapToDismiss)
        
        ReloadData()
    }
    
    
    //Keyboard Functions
    @objc func dismissKeyboard() {
        view.endEditing(true)
    }
    
    @objc func keyboardWillShow(notification: NSNotification) {
        if let keyboardSize = (notification.userInfo?[UIKeyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue {
            if self.keyBoardShown == false{
                self.keyBoardShown = true
                self.viewOrigin = self.view.frame.origin.y
                self.view.frame.origin.y -= keyboardSize.height/1.5
            }
        }
    }
    
    @objc func keyboardWillHide(notification: NSNotification) {
            if self.keyBoardShown == true{
                self.keyBoardShown = false
                self.view.frame.origin.y = self.viewOrigin
            }
    }
    
    
    //Reload the Data from the API
    func ReloadData(){
        guard let url = URL(string: jsonUrl) else
        {
            print("Can't make URL")
            return
        }
        URLSession.shared.dataTask(with: url) { (data, response, err) in
            guard let data = data else {
                self.SimulateData()
                return
            }
            do {
                let json = try? JSONDecoder().decode([Int].self, from: data)
                self.arrayOfNumbers = json!
                print(self.arrayOfNumbers)
                DispatchQueue.main.sync {
                    self.arrayCollectionView.reloadData()
                }
            }
        }.resume()
    }
    
    //Simulate the Data if there is no Internet Connection
    func SimulateData(){
        print("SImulating Data")
        self.arrayOfNumbers.removeAll()
        for _ in 1...16{
            let result = Int(arc4random_uniform(UInt32(maxValue - minValue + 1))) + minValue
            self.arrayOfNumbers.append(Int(result))
        }
        print(self.arrayOfNumbers)
        DispatchQueue.main.sync {
            self.arrayCollectionView.reloadData()
        }
    }
    
    //Compare the data with the number given
    func CompareData(array : [Int] , numberToCompare : Int) -> Bool {
        var sumAux = 0
        for (index, x) in array.enumerated(){
            for (indey, y) in array.enumerated(){
                if index != indey{
                    sumAux = x + y
                    if sumAux == numberToCompare{
                        return true
                    }
                }
            }
        }
        return false
    }
    
    //Save the data in the global variables
    func SaveData(numberToSave : Int, result : Bool){
        savedArrays.append(arrayOfNumbers)
        savedNumbers.append(numberToSave)
        savedResults.append(result)
        print(savedNumbers)
        print(savedArrays)
    }
    
    //Check the number given and compare to the array
    @IBAction func VerificarButtonTapped(_ sender: Any) {
        view.endEditing(true)
        guard let numberText = textField.text else { return }
        guard let numberToCompare = Int(numberText) else {
            showToast(message: "Número inválido")
            textField.text = ""
            return }
        if numberToCompare > -198 && numberToCompare < 198{
            let isTrue = CompareData(array: arrayOfNumbers, numberToCompare: numberToCompare)
            SaveData(numberToSave: numberToCompare, result: isTrue)
            if isTrue{
                showToast(message: "Existe")
            } else {
                showToast(message: "Não Existe")
            }
        } else{
            showToast(message: "Número inválido")
        }
        textField.text = ""
    }
    
    //Reload button
    @IBAction func ReloadTheList(_ sender: Any) {
        self.ReloadData()
        view.endEditing(true)
        showToast(message: "Recarregando Lista")
    }

    //Collection View DataSource
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 16
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        var cell: UICollectionViewCell
        
        cell = collectionView.dequeueReusableCell(withReuseIdentifier: "numberCell", for: indexPath)
        let auxCell = cell as! CollectionViewCell
        if self.arrayOfNumbers.count > indexPath.row {
            auxCell.cellLabel.text = String(self.arrayOfNumbers[indexPath.row])
        }
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let inset = self.view.frame.size.width/5
        return CGSize(width: inset, height: inset/1.5)
    }
}

extension UIViewController {
    
    //Toast Manager
    func showToast(message : String) {
        
        let toastLabel = UILabel(frame: CGRect(x: self.view.frame.size.width/5, y: self.view.frame.size.height-60, width: 200, height: 40))
        toastLabel.backgroundColor = UIColor.black.withAlphaComponent(0.5)
        toastLabel.textColor = UIColor.white
        toastLabel.textAlignment = .center;
        toastLabel.font = UIFont(name: "System-System", size: 12.0)
        toastLabel.text = message
        toastLabel.alpha = 1.0
        toastLabel.layer.cornerRadius = 10;
        toastLabel.clipsToBounds  =  true
        self.view.addSubview(toastLabel)
        UIView.animate(withDuration: 3.0, delay: 1.0, options: .curveEaseOut, animations: {
            toastLabel.alpha = 0.0
        }, completion: {(isCompleted) in
            toastLabel.removeFromSuperview()
        })
    }
}

