//
//  CollectionViewCell.swift
//  TesteMobile
//
//  Created by Vitor Zitti on 7/13/18.
//  Copyright © 2018 Zitti. All rights reserved.
//

import Foundation
import UIKit

class CollectionViewCell : UICollectionViewCell{
    //Label of the collection view using the array of numbers
    @IBOutlet weak var cellLabel: UILabel!
}
