//
//  TesteMobileTests.swift
//  TesteMobileTests
//
//  Created by Vitor Zitti on 7/12/18.
//  Copyright © 2018 Zitti. All rights reserved.
//

import XCTest
@testable import TesteMobile

class TesteMobileTests: XCTestCase {
    
    let mainView = ViewController()
    
    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    func testCompareData(){
        let num = 3
        let num2 = 100
        let numArray = [1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16]
        
        let resultTrue = mainView.CompareData(array: numArray, numberToCompare: num)
        let resultFalse = mainView.CompareData(array: numArray, numberToCompare: num2)
        
        XCTAssert(resultTrue == true)
        XCTAssert(resultFalse == false)
    }
    
}
