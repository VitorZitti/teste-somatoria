//
//  HistoryViewController.swift
//  TesteMobile
//
//  Created by Vitor Zitti on 7/16/18.
//  Copyright © 2018 Zitti. All rights reserved.
//

import Foundation
import UIKit

class HistoryViewController: UIViewController ,UITableViewDataSource, UITableViewDelegate {
    
    let storyBoard = UIStoryboard.init(name: "Main", bundle: nil)
     var popUpVC: PopUpViewController!
    
    @IBOutlet var tableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.popUpVC = self.storyBoard.instantiateViewController(withIdentifier: "PopUpID") as! PopUpViewController
        tableView.dataSource = self
        tableView.delegate = self
        tableView.reloadData()
    }
    
    //Table View DataSource
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return savedNumbers.count + 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.row == 0{
            let cell = tableView.dequeueReusableCell(withIdentifier: "TitleCell", for: indexPath as IndexPath)
            return cell
        }else {
            let cell = tableView.dequeueReusableCell(withIdentifier: "HistoryCell", for: indexPath as IndexPath) as! HistoryCell
            cell.numberLabel.text = String(savedNumbers[indexPath.row - 1])
            if savedResults[indexPath.row - 1] == true{
                cell.resultLabel.text = "Existe"
            } else{
                cell.resultLabel.text = "Não Existe"
            }
            return cell
        }
    }
    
    //Create the PopUp
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.row > 0{
            self.addChildViewController(self.popUpVC)
            self.popUpVC.view.frame = self.view.frame
            self.view.addSubview(self.popUpVC.view)
            self.popUpVC.didMove(toParentViewController: self)
            self.popUpVC.arrayPosition = indexPath.row - 1
            self.popUpVC.arrayCollectionView.reloadData()
        }
    }
}
