//
//  HistoryCell.swift
//  TesteMobile
//
//  Created by Vitor Zitti on 7/16/18.
//  Copyright © 2018 Zitti. All rights reserved.
//

import Foundation
import UIKit

class HistoryCell : UITableViewCell{
    //Labels from the history check Cell
    @IBOutlet weak var numberLabel: UILabel!
    @IBOutlet weak var resultLabel: UILabel!
}
