//
//  PopUpViewController.swift
//  TesteMobile
//
//  Created by Vitor Zitti on 7/16/18.
//  Copyright © 2018 Zitti. All rights reserved.
//

import Foundation
import UIKit

class PopUpViewController : UIViewController, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout{
    
    var arrayPosition : Int = 0
    @IBOutlet weak var arrayCollectionView: UICollectionView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.arrayCollectionView.delegate = self
        self.arrayCollectionView.dataSource = self
    }
    
    //Collection View DataSource
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 16
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        var cell: UICollectionViewCell
        
        cell = collectionView.dequeueReusableCell(withReuseIdentifier: "numberCell", for: indexPath)
        let auxCell = cell as! CollectionViewCell
        if savedArrays[arrayPosition].count > indexPath.row {
            auxCell.cellLabel.text = String(savedArrays[arrayPosition][indexPath.row])
        }
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let inset = self.view.frame.size.width/6
        return CGSize(width: inset, height: inset/1.5)
    }
    
    //Close The PopUp
    @IBAction func CloseTapped(_ sender: Any) {
        self.view.removeFromSuperview()
        self.removeFromParentViewController()
    }
}
