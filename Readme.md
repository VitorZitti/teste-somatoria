# Teste de somatória. #

*Recebe de uma API um array de 16 números entre -99 e 99.

*Compara se a somatória de qualquer par desses números se iguala a um número digitado pelo usuátio.

*O número digitado deve estar entre  -197 e 197.

*Usa toast para confirmar se existe ou não uma combinação que resulte no número digitado.

*Tela de histórico de consulta com os números digitados e os arrays usados na hora da digitação.